![image.png](https://media.discordapp.net/attachments/895994961462448168/1050378932865740830/image.png)

## 📑 Description

- This is nest.js API

## ⚙️ General information

| Dependency name     | Dependency version                                  |
| ------------------- | --------------------------------------------------- |
| 🧠 @nestjs/common   | ^9.0.0 |
| 🧠 @nestjs/core     | ^9.0.0 | 
| 🧠 @nestjs/platform-express   | ^9.0.0 |
| 🧠 @nestjs/typeorm  | ^9.0.1 |
| 🧩 class-transformer| ^0.5.1 | 
| 🧩 class-validator  | ^0.13.2 |
| 🧩 mysql            | ^2.18.1 |
| 🧩 reflect-metadata | ^0.1.13 | 
| 🧩 rimraf           | ^3.0.2 |
| 🧩 rxjs             | ^7.2.0 |
| 🧩 typeorm          | ^0.3.10 |
