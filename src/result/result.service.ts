import { Injectable, NotFoundException } from "@nestjs/common";
import { Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm";
import { result_data } from "src/enities/result_data";

@Injectable()
export class ResultService {
  constructor(
    @InjectRepository(result_data)
    private readonly resultRepos: Repository<result_data>
  ) {}

  public async getStatus(wellId: number, position: number) {
    const sortByWellId = await this.resultRepos.findAndCount({
      where: { well_bore_id: { id: wellId } },
      relations: ["well_bore_id"],
      skip: position,
      take: 1,
    });
    if (sortByWellId) {
      return {
        data: sortByWellId[0],
        count: sortByWellId[1],
      };
    } else {
      throw new NotFoundException("No results found");
    }
  }
}
