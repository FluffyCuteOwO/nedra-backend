import { Module } from "@nestjs/common";
import { ResultService } from "./result.service";
import { ResultController } from "./result.controller";
import { TypeOrmModule } from "@nestjs/typeorm";
import { result_data } from "src/enities/result_data";

@Module({
  imports: [TypeOrmModule.forFeature([result_data])],
  providers: [ResultService],
  controllers: [ResultController],
})
export class ResultModule {}
