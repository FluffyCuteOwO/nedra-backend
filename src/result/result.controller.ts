import { Controller, Get, Param } from "@nestjs/common";
import { ResultService } from "./result.service";

@Controller("result")
export class ResultController {
  constructor(private readonly resultService: ResultService) {}

  @Get("/:wellId/:id")
  getById(@Param("wellId") wellId, @Param("id") position) {
    return this.resultService.getStatus(wellId, position);
  }
}
