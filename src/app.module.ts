import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { result_data } from "./enities/result_data";
import { well_bore } from "./enities/well_bore";
import { WellsModule } from "./wells/wells.module";
import { ResultModule } from "./result/result.module";
import { username, password, host } from "./config";
import { well } from "./enities/well";
import { well_bore_status } from "./enities/well_bore_status";

@Module({
  imports: [
    WellsModule,
    TypeOrmModule.forRoot({
      type: "mysql",
      host: host,
      port: 3306,
      username: username,
      password: password,
      database: "new_site",
      entities: [well, well_bore_status, well_bore, result_data],
      synchronize: false,
    }),
    ResultModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
