import { Injectable, NotFoundException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { well_bore } from "src/enities/well_bore";
import { Repository } from "typeorm";

@Injectable()
export class WellsService {
  constructor(
    @InjectRepository(well_bore)
    private readonly well_bore_db: Repository<well_bore>
  ) {}

  public async getWells() {
    const db = await this.well_bore_db.find();
    if (db) {
      return db;
    } else {
      throw new NotFoundException("No wells found");  // <--- This is the error
    }
  }
}
