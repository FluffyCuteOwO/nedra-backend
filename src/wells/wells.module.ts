import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { well_bore } from "src/enities/well_bore";
import { WellsController } from "./wells.controller";
import { WellsService } from "./wells.service";

@Module({
  imports: [TypeOrmModule.forFeature([well_bore])],
  controllers: [WellsController],
  providers: [WellsService],
})
export class WellsModule {}
