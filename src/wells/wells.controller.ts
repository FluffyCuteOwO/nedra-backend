import { Controller, Get } from "@nestjs/common";
import { WellsService } from "./wells.service";

@Controller("wells")
export class WellsController {
  constructor(private readonly wellsService: WellsService) {}

  @Get()
  getWells() {
    return this.wellsService.getWells();
  }
}
