import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { well_bore } from "./well_bore";

@Entity()
export class well {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @OneToMany(() => well_bore, (data) => data.well_id)
  well: well[];
}
