import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { well } from "./well";
import { result_data } from "./result_data";
import { well_bore_status } from "./well_bore_status";

@Entity()
export class well_bore extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @ManyToOne(() => well, (data) => data.id)
  @JoinColumn({ name: `well_id` })
  well_id: well;

  @ManyToOne(() => well_bore_status, (data) => data.id)
  @JoinColumn({ name: `status_id` })
  status_id: well_bore_status;

  @Column()
  to_control: number;

  @Column()
  set_control_date: Date;

  @Column()
  user_id: number;

  @OneToMany(() => result_data, (data) => data.well_bore_id)
  result_data: result_data[];
}
