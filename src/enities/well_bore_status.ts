import {
  BaseEntity,
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  Unique,
} from "typeorm";
import { well_bore } from "./well_bore";

@Entity()
export class well_bore_status extends BaseEntity {
  @PrimaryGeneratedColumn()
  @Unique("ix_well_bore_status_id", [`id`])
  id: number;

  @Unique("name", [`name`])
  @Column({
    type: "varchar",
    length: 200,
    nullable: true,
  })
  name: string;

  @OneToMany(() => well_bore, (data) => data.status_id)
  well_bore_status: well_bore_status[];
}
