import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { well_bore } from "./well_bore";

@Entity()
export class result_data {
  @PrimaryGeneratedColumn()
  id: number;

  @Column("datetime")
  time: Date;

  @Column("float")
  P_ca: number;

  @Column("float")
  Q_ca: number;

  @Column("float")
  P_tr: number;

  @Column("float")
  P_zatr: number;

  @Column("float")
  P_dikt: number;

  @Column("float")
  T_izm: number;

  @Column("float")
  operating_mode_P_tr: number;

  @Column("float")
  operating_mode_P_zatr: number;

  @Column("float")
  diff_T_izm: number;

  @Column("float")
  origin_P_zatr: number;

  @Column({ type: "tinyint", width: 1 })
  closed: number;

  @Column({ type: "tinyint", width: 1 })
  purging: number;

  @Column({ type: "tinyint", width: 1 })
  gas_hydrate: number;

  @Column({ type: "tinyint", width: 1 })
  liquid_release_hydrate: number;

  @Column({ type: "tinyint", width: 1 })
  liquid_release: number;

  @Column({ type: "int", width: 11 })
  big_liquid_release: number;

  @Column({ type: "int", width: 11 })
  closed_time: number;

  @Column({ type: "int", width: 11 })
  work_time: number;

  @Column({ type: "int", width: 11 })
  delay_before_operating_mode: number;

  @Column({ type: "int", width: 11 })
  attention_time: number;

  @ManyToOne(() => well_bore, (data) => data.id)
  @JoinColumn({ name: `well_bore_id` })
  well_bore_id: well_bore;
}
